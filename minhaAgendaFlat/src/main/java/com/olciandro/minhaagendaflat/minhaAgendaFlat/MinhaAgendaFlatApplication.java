package com.olciandro.minhaagendaflat.minhaAgendaFlat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinhaAgendaFlatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinhaAgendaFlatApplication.class, args);
	}
}
